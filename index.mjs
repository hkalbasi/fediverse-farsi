import { writeFile } from "fs";
import { fetchAct, fetchOrderedCollection } from "./network.mjs";

process.on('unhandledRejection', (up) => { throw up; });

const containPersian = (text) => {
  const p = /^.*[\u0600-\u06FF].*$/;
  return p.test(text);
};

const isPersian = (user) => {
  if (!user.summary) return false;
  if (containPersian(user.summary)) {
    return true;
  }
  return false;
};

const me = 'https://norden.social/users/hamid';

let cnt = 0;
const mark = {};
const meta = {};

// 824 founded

const crawl = async (url) => {
  if (mark[url]) return;
  mark[url] = true;
  const user = await fetchAct(url);
  const p = isPersian(user);
  if (!p) return;
  cnt += 1;
  console.log(`${cnt}. ${p} ${url}`);
  const me = { url, following: [], followers: [] };
  if (user.following){
    const folgc = await fetchAct(user.following);
    me.following = (await fetchOrderedCollection(folgc)) || [];
  }
  if (user.followers){
    const folgc = await fetchAct(user.followers);
    me.followers = (await fetchOrderedCollection(folgc)) || [];
  }
  me.followers.forEach(crawl);
  me.following.forEach(crawl);
  meta[url] = me;
  writeFile('meta.json', JSON.stringify(meta), ()=>{});
  //console.log(user.summary);
};

const main = async () => {
  crawl(me);
};

main();
