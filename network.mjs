import fetch from "node-fetch";

const delay = (ms) => new Promise(res=>setTimeout(res, ms));

const queriesDB = {};

const queryHandler = async (host) => {
  let head = 0;
  const queries = queriesDB[host];
  while (true) {
    await delay(1000);
    if (head === queries.length) continue;
    const q = queries[head];
    try {
      const res = await fetch(q.url, {
        headers: {
          'Accept': 'application/activity+json',
        },
      });
      q.resolve(await res.json());
    }
    catch (e) {
      q.resolve({ fail: true, reason: e });
    }
    //console.log(q.url+' fetched');
    head += 1;
  }
};

export const fetchAct = async (url) => {
  if (url === undefined) return { fail: true, reason: 'bad param' };
  return new Promise((resolve) => {
    const host = url.split('/')[2];
    if (!queriesDB[host]) {
      queriesDB[host] = [{ url, resolve }];
      queryHandler(host);
    }
    else{
      queriesDB[host].push({ url, resolve });
    }
  });
};

export const fetchOrderedCollection = async (c) => {
  let fr = await fetchAct(c.first);
  const res = fr.orderedItems;
  try {
    while (fr.next) {
      fr = await fetchAct(fr.next);
      res.push(...fr.orderedItems);
    }
    return res;
  }
  catch (e) {
    return [];
  }
};
