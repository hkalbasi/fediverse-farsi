import fs from "fs";
import { promisify } from "util";
import { fetchAct } from "./network.mjs";
import { fetchOrderedCollection } from "./network.mjs";

const readFile = promisify(fs.readFile);
const writeFile = promisify(fs.writeFile);

const delay = (ms) => new Promise(res=>setTimeout(res, ms));

const backup = async (result) => {
  while (true) {
    await delay(1000);
    await writeFile('last.json', JSON.stringify(result));
  }
};

const main = async () => {
  const result = {};
  backup(result);
  const meta = JSON.parse((await readFile('meta.json')).toString());
  const users = Object.keys(meta);
  let i = 0;
  await Promise.all(await users.map(async (x)=>{
    const ua = await fetchAct(x);
    const out = await fetchOrderedCollection(await fetchAct(ua.outbox));
    if (out.length < 1) return;
    result[x] = out[0].published;
    i += 1;
    console.log(i);
  }));
};

main();
